$(function () {

    var input_popover = $('.input-popover');

    $(document).on('click', '.hotel-photo-list a', function () {
        $(this).parents('.hotel-photo-list').find('li').removeClass('active').eq($(this).parent().index()).addClass('active');

        $(this).parents('.hotel-view').find('.hotel-photo').attr('src', $(this).find('img').data('image-big'));
    });

    $('.js-side-menu-toggle').on('click', function (e) {
        e.preventDefault();

        $('.js-side-menu-toggle').toggle();
        $('.side-menu-block').toggle();
    });

    $('select').select2();

    $('input.daterange').daterangepicker({
        format: 'DD.MM.YYYY',
        locale: {
            applyLabel: 'OK',
            cancelLabel: 'Отменить',
            fromLabel: 'От',
            toLabel: 'До',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            firstDay: 1
        }
    });

    input_popover.each(function () {
        var content = $(this).next().html();

        $(this).popover({
            html: true,
            "content": content,
            placement: 'bottom',
            trigger: 'click'
        });
    });

    input_popover.on('show.bs.popover', function () {
        $(this).next().html('');
    });

});